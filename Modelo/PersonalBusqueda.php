<?php
	require_once("../Modelo/Conexion.php");
	class personalBusqueda
	{
		private $conexion;

		function __construct()
		{
			$this->conexion =  new Conexion();
		}

	
     public function verificarUsuarioEstudiante($usuario)
        {   
            $sqlverificarUsuarioEstudiante = "SELECT * FROM estudiante WHERE usuario=:usuario ;";
		            $cmd = $this->conexion->prepare($sqlverificarUsuarioEstudiante);
		            $cmd->bindParam(':usuario',$usuario);
		            $cmd->execute();
            $verificarUsuarioEstudiante= $cmd->fetch();
                        if($verificarUsuarioEstudiante){
               return $verificarUsuarioEstudiante;
            }else{
                return 0;
            }
        }
           public function verificarUsuarioPersonal($usuario)
        {   
            $sqlverificarUsuarioPersonal = "SELECT * FROM personal WHERE usuario=:usuario ;";
                    $cmd = $this->conexion->prepare($sqlverificarUsuarioPersonal);
                    $cmd->bindParam(':usuario',$usuario);
                    $cmd->execute();
            $verificarUsuarioPersonal= $cmd->fetch();
                        if($verificarUsuarioPersonal){
               return $verificarUsuarioPersonal;
            }else{
                return 0;
            }
        }
        public function verificarContraseniaPersonal($contrasenia)
        {  
            
            $sqlverificarContraseniaPersonal = "SELECT * FROM personal WHERE contrasenia=:contrasenia ;";
            $cmd = $this->conexion->prepare($sqlverificarContraseniaPersonal);
                
                $cmd->bindParam(':contrasenia',$contrasenia);
                $cmd->execute();
                $verificarContraseniaPersonal= $cmd->fetch();
            
            if($verificarContraseniaPersonal){
               return 1;
            }else{
                return 0;
            }
        }
                public function verificarContraseniaEstudiante($contrasenia)
        {  
            
            $sqlverificarContraseniaEstudiante = "SELECT * FROM estudiante WHERE contrasenia=:contrasenia ;";
            $cmd = $this->conexion->prepare($sqlverificarContraseniaEstudiante);
                
                $cmd->bindParam(':contrasenia',$contrasenia);
                $cmd->execute();
                $verificarContraseniaEstudiante= $cmd->fetch();
            
            if($verificarContraseniaEstudiante){
               return $verificarContraseniaEstudiante;
            }else{
                return 0;
            }
        }
    

       public function rolEstudiante($usuario)
        { 
            $sqlrolEstudiante = "SELECT *,CONCAT_WS(' ',primerNombre,segundoNombre,apellidoPaterno,apellidoMaterno) AS nombreUsuario FROM estudiante where usuario=:usuario;";
                    $cmd = $this->conexion->prepare($sqlrolEstudiante);
                    $cmd->bindParam(':usuario',$usuario);
                    $cmd->execute();
                    $rolEstudiante= $cmd->fetch();
            if($rolEstudiante){
               return $rolEstudiante;
            }else{
                return 0;
            }
        }
    public function rolPersonal($usuario)
        { 
            $sqlrolPersonal = "SELECT *,CONCAT_WS(' ',primerNombre,segundoNombre,apellidoPaterno,apellidoMaterno) AS nombreUsuario FROM personal where usuario=:usuario;";
                    $cmd = $this->conexion->prepare($sqlrolPersonal);
                    $cmd->bindParam(':usuario',$usuario);
                    $cmd->execute();
                    $rolPersonal= $cmd->fetch();
            if($rolPersonal){
               return $rolPersonal;
            }else{
                return 0;
            }
        }

       public function datosEstudiante($usuario)
        {   
            $sqlDatosEstudiante = "SELECT * FROM estudiante WHERE usuario=:usuario ;";
                    $cmd = $this->conexion->prepare($sqlDatosEstudiante);
                    $cmd->bindParam(':usuario',$usuario);
                    $cmd->execute();
            $DatosEstudiante= $cmd->fetch();
                        if($DatosEstudiante){
               return $DatosEstudiante;
            }else{
                return 0;
            }
        }
           public function datosPersonal($usuario)
        {   
            $sqlDatosPersonal = "SELECT * FROM personal WHERE usuario=:usuario ;";
                    $cmd = $this->conexion->prepare($sqlDatosPersonal);
                    $cmd->bindParam(':usuario',$usuario);
                    $cmd->execute();
            $DatosPersonal= $cmd->fetch();
                        if($DatosPersonal){
               return $DatosPersonal;
            }else{
                return 0;
            }
        }

        public function listaEstudiantesBecasGestion($idGestion)
        {   //realizando la consulta
            $sqlListaEstudianteBecasGestion = "
                SELECT g.nombre as gestion,count(e.idEstudiante) as cantEstudiantes
                from estudiante e inner join becaNoInstitucional bni
                on bni.idEstudiante= e.idEstudiante
                inner join gestion g
                on g.idGestion=bni.idGestion
                AND g.idGestion=:idGestion
                inner join tipoBeca tb
                on tb.idTipoBeca=bni.idTipoBeca
                group by g.nombre;

            ";
            $cmd = $this->conexion->prepare($sqlListaEstudianteBecasGestion);
            $cmd->bindParam(':idGestion', $idGestion);
            $cmd->execute();
            $listaEstudiantesConsulta = $cmd->fetchAll();
            return $listaEstudiantesConsulta;
        }

         public function listaReporte2($idGestion)
        {  
            $sqlListaReporte2 = "
                SELECT  g.nombre as gestion  ,tb.nombre as tipoBeca ,count(e.idEstudiante)   as  estudiante 
                from carrera c inner join estudiante e 
                on e.idCarrera =c.idCarrera
                inner JOIN  becaNoInstitucional bni 
                on e.idEstudiante=bni.idEstudiante
                inner join gestion g
                on g.idGestion =bni.idGestion
                AND g.idGestion=:idGestion
                inner join tipoBeca tb
                on tb.idTipoBeca=bni.idTipoBeca
                group by  g.idGestion
                having count(e.idEstudiante)>=2;

            ";
            $cmd = $this->conexion->prepare($sqlListaReporte2);
            $cmd->bindParam(':idGestion', $idGestion);
            $cmd->execute();
            $listaReporte1Consulta = $cmd->fetchAll();
            return $listaReporte1Consulta;
        }   

	 public function listaAreas()
    {
    $sqlListaArea ="SELECT  idArea,nombre from area order by nombre;";
     $cmd = $this->conexion->prepare($sqlListaArea);
     $cmd->execute();
     $listaConsulta = $cmd->fetchAll();
    return $listaConsulta;
    }
    
     public function listaGestion()
    {
    $sqlListaGestion ="SELECT  idGestion,nombre from Gestion where activo=1 ;";
     $cmd = $this->conexion->prepare($sqlListaGestion);
     $cmd->execute();
     $listaConsulta = $cmd->fetchAll();
    return $listaConsulta;
    }
     public function listaPrecio()
    {
    $sqlListaPrecio ="SELECT  idPrecio,precio from precio order by precio;";
     $cmd = $this->conexion->prepare($sqlListaPrecio);
     $cmd->execute();
     $listaConsulta = $cmd->fetchAll();
    return $listaConsulta;
    }
     public function listaDia()
    {
    $sqlListaDia ="SELECT  idDia,dia from dia ;";
     $cmd = $this->conexion->prepare($sqlListaDia);
     $cmd->execute();
     $listaConsulta = $cmd->fetchAll();
    return $listaConsulta;
    }
     public function listaHorarioTrabajo()
    {
    $sqlListaHorarioTrabajo ="SELECT  a.nombre as nombreArea ,g.nombre as nombreGestion,p.precio as precio
                                FROM becainstitucional bi INNER JOIN area a 
                                 ON bi.idArea = a.idArea 
                                 INNER join gestion g 
                                 on bi.idGestion=g.idGestion
                                 INNER JOIN precio p
                                 on bi.idPrecio=p.idPrecio
                                 WHERE idBecaInstitucional = (SELECT MAX(idBecaInstitucional) as maxid FROM becainstitucional);";
     $cmd = $this->conexion->prepare($sqlListaHorarioTrabajo);
     $cmd->execute();
     $listaConsulta = $cmd->fetch();
    return $listaConsulta;
    }
    



    }
?>